package pl.edu.pwsztar.domain;

import static java.util.Objects.requireNonNull;

class MovieCreator {

    Movie from(MovieDto movieDto) {
        requireNonNull(movieDto);
        return Movie.builder()
                .movieId(movieDto.getMovieId())
                .title(movieDto.getTitle())
                .image(movieDto.getImage())
                .id(movieDto.getId())
                .build();
    }
}
