package pl.edu.pwsztar.domain;

import lombok.*;


@Builder(toBuilder = true)
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MovieDto {

    private Long movieId;
    private String title;
    private String image;
    private Integer year;
    private String videoId;
}
