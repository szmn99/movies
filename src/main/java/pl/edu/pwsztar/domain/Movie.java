package pl.edu.pwsztar.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long movieId;
    private String title;
    private String image;
    private Integer year;

    @Column(name = "video_id")
    private String videoId;

    MovieDto dto() {
        return MovieDto.builder()
                .movieId(this.movieId)
                .title(this.title)
                .image(this.image)
                .year(this.year)
                .videoId(this.videoId)
                .build();
    }
}