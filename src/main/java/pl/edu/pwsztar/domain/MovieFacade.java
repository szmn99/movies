package pl.edu.pwsztar.domain;

import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

@Transactional
public class MovieFacade {
    private final MovieRepository movieRepository;
    private final MovieCreator movieCreator;

    public MovieFacade(MovieRepository movieRepository, MovieCreator movieCreator) {
        this.movieRepository = movieRepository;
        this.movieCreator = movieCreator;
    }

    public void create(CreateMovieDto createMovieDto){
        requireNonNull(createMovieDto);
        movieRepository.createMovie(createMovieDto);

    }

    public MovieDto update(MovieDto movieDto) {
        requireNonNull(movieDto);
        Movie movie = movieRepository.findOneOrThrow(movieDto.getId());
        movie =  movieRepository.save(movie.toBuilder().title(movieDto.getTitle()).build());
        return movie.dto();
    }

    public MovieDto show(Long id) {
        requireNonNull(id);
        Movie movie = movieRepository.findOneOrThrow(id);
        return movie.dto();
    }

    public void delete(Long movieId ) {
        requireNonNull(movieId);
        movieRepository.deleteMovie(movieId);
    }

    public MovieCounterDto countMovies(){
        MovieCounterDto movieCounterDto = movieCreator.from(movieRepository.countMovies());
        return movieCounterDto;

    }
    public List<MovieDto> findAll() {

        return movieRepository
                .findAll().stream()
                .map(Movie::dto)
                .collect(Collectors.toList());
    }
}
