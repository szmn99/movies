package pl.edu.pwsztar.domain;

public class MovieNotFoundException extends RuntimeException{
    public MovieNotFoundException(Long id) {
        super("No film of id " + id + " found", null, false, false);
    }
}
