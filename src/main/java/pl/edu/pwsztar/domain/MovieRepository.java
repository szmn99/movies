package pl.edu.pwsztar.domain;

import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.UpdateMovieDto;

import java.util.List;

public interface MovieRepository {


    List<Movie> findAll();
    void createMovie(CreateMovieDto createMovieDto);
    void deleteMovie(Long movieId);
    Movie findMovie(Long movieId);
    Long countMovies();
    void updateMovie(Long movieId, UpdateMovieDto updateMovieDto);

}